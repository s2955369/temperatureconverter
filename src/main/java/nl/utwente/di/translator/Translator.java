package nl.utwente.di.translator;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

public class Translator extends HttpServlet {

    private Temperature temperature;

    public void init() throws ServletException {
       temperature = new Temperature();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Web Celsius to Fahrenheit Converter";
        String result = temperature.calculate(request.getParameter("celsius"));
        String button = "<a href=\"index.html\"><button>Go back</button></a>";

        if (result != null) {
            out.println("<!DOCTYPE HTML>\n" +
                    "<HTML>\n" +
                    "<HEAD><TITLE>" + title + "</TITLE>" +
                    "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                    "</HEAD>\n" +
                    "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                    "<H1>" + title + "</H1>\n" +
                    "  <P>The temperature in Celsius that you entered " +
                    request.getParameter("celsius") + " translates to\n" +
                    "  <P>Fahrenheit: " +
                    result + "°F</p><br>" + button +
                    "</BODY></HTML>");
        } else {
            out.println("<!DOCTYPE HTML>\n" +
            "<HTML>\n" +
                    "<HEAD><TITLE>" + title + "</TITLE>" +
                    "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                    "</HEAD>\n" +
                    "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                    "<H1>" + title + "</H1>\n" +
                    "  <P>It looks like you entered a string, not a number. You entered \"" +
                    request.getParameter("celsius") + "\".\n" +
                    " Please enter a valid number! </p><br>" + button +
                    "</BODY></HTML>");
        }
    }

}
