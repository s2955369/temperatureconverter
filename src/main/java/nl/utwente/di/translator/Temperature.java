package nl.utwente.di.translator;

public class Temperature {

    public String calculate(String temperature) {
        try {
            double number = Double.parseDouble(temperature);
            return String.valueOf((number * 9 / 5) + 32);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
