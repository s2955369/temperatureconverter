import nl.utwente.di.translator.Temperature;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TemperatureTest {

    private Temperature temperature;

    @BeforeEach
    public void setUp() {
        temperature = new Temperature();
    }
    @Test
    public void test() {
        String expected = "33.8";
        String test = "test";
        Assertions.assertEquals(expected, temperature.calculate(String.valueOf(1)), "Temperature converted");
        Assertions.assertNull(temperature.calculate(test));
    }
}
